import React from "react";
import styled, { ThemeProvider } from "styled-components";
import WishList from "../components/WishList";

interface Theme {
  foreground: string;
  background: string;
  backgroundAlt: string;
  backgroundAlt2: string;
}

/*const lightTheme: Theme = {
  foreground: "black",
  background: "white",
  backgroundAlt: "#eee",
};*/
const darkTheme: Theme = {
  foreground: "#eee",
  background: "#222",
  backgroundAlt: "#333",
  backgroundAlt2: "#444",
};

const Page = styled.div`
  width: 100%;
  height: 100%;
  min-height: 100vh;
  color: ${(props) => `${props.theme.foreground}`};
  background-color: ${(props) => `${props.theme.background}`};
`;

const Container = styled.div`
  width: 95%;
  margin: 0 auto;
  transition: width 100ms ease;
  padding: 5px 0;

  @media only screen and (min-width: 1000px) {
    width: 60%;
    padding: 20px 0;
  }
`;

const Layout: React.FC = (props) => (
  <ThemeProvider theme={darkTheme}>
    <Page>
      <Container>{props.children}</Container>
    </Page>
  </ThemeProvider>
);

const VerticalSpacer = styled.div`
  margin-top: 60px;
`;

export default function Home() {
  return (
    <Layout>
      <style jsx global>
        {`
          *,
          button {
            font-family: "Inconsolata", monospace;
            margin: 0;
            padding: 0;
          }
          body,
          html {
            margin: 0;
            padding: 0;
          }

          h1,
          h2,
          h3,
          h4,
          h5,
          h6,
          p,
          span,
          strong {
            padding: 0;
            margin: 0;
          }
        `}
      </style>
      <h1 style={{ textAlign: "center" }}>Kasper's Christmas Wishlist 2020</h1>
      <VerticalSpacer />
      <WishList />
    </Layout>
  );
}
