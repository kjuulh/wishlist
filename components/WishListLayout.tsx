import styled from "styled-components";

export const WishListLayout = styled.div`
  display: flex;
  flex-flow: column;
  gap: 20px;
`;
