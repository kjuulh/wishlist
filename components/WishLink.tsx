import React from "react";
import { useRouter } from "next/router";
import { Wish } from "./Wish";
import { WishLinkImageContainer } from "./WishLinkImageContainer";
import { WishLinkCard } from "./WishLinkCard";
import { WishLinkImage } from "./WishLinkImage";
import styled from "styled-components";

export const WishLinkBackground = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  background-color: ${(props) => props.theme.backgroundAlt};
  z-index: 10;
  border-radius: 10px;
`;

export const WishButton = styled.button`
  z-index: 20;
  padding: 15px 30px;
  font-size: 1.2rem;
  text-transform: uppercase;
  background-color: ${(props) => props.theme.background};
  border: none;
  color: ${(props) => props.theme.foreground};
  outline: none;
  border-radius: 8px;
  transition: background-color 200ms ease;

  :hover {
    background-color: ${(props) => props.theme.backgroundAlt2};
  }

  :active {
    background-color: ${(props) => props.theme.backgroundAlt};
  }
`;

const WishLinkText = styled.p`
  display: block;
  word-break: break-word;
  display: none;
  @media only screen and (min-width: 1000px) {
    display: inline;
  }
`;

const WishLinkName = styled.strong`
  display: block;
  font-size: 14px;
  margin-bottom: 3px;
  @media only screen and (min-width: 1000px) {
    font-size: 16px;
  }
`;

export const WishLink: React.FC<Wish> = (props) => {
  const router = useRouter();
  if (props.pictureUrl) {
    return (
      <WishLinkCard>
        <WishLinkBackground onClick={() => router.push(props.link)} />
        <WishLinkImageContainer>
          <WishLinkImage src={props.pictureUrl} height={100} width={100} />
          <div>
            <WishLinkName style={{ marginBottom: "3px" }}>
              {props.name}
            </WishLinkName>
            <WishLinkText>{props.link}</WishLinkText>
          </div>
        </WishLinkImageContainer>
        <WishButton onClick={() => navigator.clipboard.writeText(props.link)}>
          Copy
        </WishButton>
      </WishLinkCard>
    );
  }

  return (
    <WishLinkCard>
      <WishLinkBackground onClick={() => router.push(props.link)} />
      <WishLinkImageContainer>
        <div>
          <WishLinkName style={{ marginBottom: "3px" }}>
            {props.name}
          </WishLinkName>
          <WishLinkText>{props.link}</WishLinkText>
        </div>
      </WishLinkImageContainer>
      <WishButton onClick={() => navigator.clipboard.writeText(props.link)}>
        Copy
      </WishButton>
    </WishLinkCard>
  );
};
