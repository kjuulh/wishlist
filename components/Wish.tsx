export interface Wish {
    name: string;
    link: string;
    pictureUrl?: string;
}