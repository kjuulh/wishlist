import React from "react";
import { WishLink } from "./WishLink";
import { WishListLayout } from "./WishListLayout";
import { wishes } from "./wishes";

const WishList = () => {
  const generateWishes = () =>
    wishes.map((wish) => <WishLink key={wish.name} {...wish} />);

  return <WishListLayout>{generateWishes()}</WishListLayout>;
};

export default WishList;
