import { Wish } from "./Wish";

export const wishes: Wish[] = [
  {
    name: "Deskmat",
    link:
      "https://cdon.dk/hjem-have/dobbeltsidet-pu-laeder-pa-skrivebordet-90x40-cm-p50721810?gclid=CjwKCAiA5IL-BRAzEiwA0lcWYsDvjUYsJW9uHmUIBtzKhdgThevC6T4iWbqgMm8q9A6KHg4Yn2t_VBoClugQAvD_BwE&gclsrc=aw.ds#fo_c=1923&fo_k=d8997330089ec31594d8b33180e10bf8&fo_s=gpladk",
    pictureUrl: "",
  },
  {
    name: "Sous Vide kar",
    link:
      "https://www.sousvide.dk/produkter/30-sous-vide-tilbehoer/857-sous-vide-kar-20-liter-m-laag/",
    pictureUrl: "https://shop5634.hstatic.dk/upload_dir/shop/anova-20liter.jpg",
  },

  {
    name: "Silokone bagemaatte",
    link:
      "https://www.imerco.dk/oxo-silikone-bagemaatte?id=100397356&utm_medium=cpc&utm_source=google&utm_campaign=1463016910&gclid=CjwKCAiA5IL-BRAzEiwA0lcWYvi1zzuf3-_zY4sDyn16EHZpBb6GWWAEFNtfDA7C6241BX-QSFXwOBoCFsEQAvD_BwE",
    pictureUrl:
      "https://cdn.imercostatic.dk/produktbilleder/oxo-silikone-bagemaatte-l-42-x-b295-cm-silikone.jpg?i=0719812682679_0719812682679_3&t=20201113105956&w=800&h=800",
  },

  {
    name: "Stegepande støbejern",
    link:
      "https://www.kitchenone.dk/produkt/lodge-stegepande-stoebejern-30-cm/",
    pictureUrl: "",
  },

  {
    name: "Google Nest Audio enten en/par",
    link:
      "https://www.proshop.dk/Smart-Home/Google-Nest-Audio-Kulsort-Nordisk/2874049",
    pictureUrl: "",
  },

  {
    name: "Keyboard",
    link: "https://www.roland.com/global/categories/keyboards/entry_keyboards/",
    pictureUrl: "",
  },

  {
    name: "Akustisk Guitar",
    link: "",
    pictureUrl: "",
  },

  {
    name: "Is maskine",
    link:
      "https://www.kitchenone.dk/ismaskiner/?gclid=CjwKCAiA5IL-BRAzEiwA0lcWYi53S4Ak1IGOS_6nDZpaHXm3afZwyanjcFtjgWerzGaVWUV421Ni_BoCqrMQAvD_BwE",
    pictureUrl: "",
  },

  {
    name: "Bog - Code Complete 2",
    link:
      "https://www.saxo.com/dk/code-complete_steve-mcconnell_paperback_9780735619678",
    pictureUrl: "",
  },

  {
    name: "Bog - Refactoring",
    link:
      "https://www.saxo.com/dk/refactoring_martin-fowler_hardback_9780134757599",
    pictureUrl: "",
  },

  {
    name: "Bog - Mythical man month",
    link:
      "https://www.saxo.com/dk/the-mythical-man-month_frederick-p-brooks-jr_paperback_9780201835953",
    pictureUrl: "",
  },

  {
    name: "Bog - Introduction to Algorithms",
    link:
      "https://www.saxo.com/dk/introduction-to-algorithms_thomas-h-cormen_paperback_9780262533058",
    pictureUrl: "",
  },

  {
    name: "Bog - Boeger som i synes har vaeret gode",
    link: "",
    pictureUrl: "",
  },

  {
    name: "Buster - Kong",
    link: "https://www.zooplus.dk/shop/hund/hundelegetoej/kong",
    pictureUrl: "",
  },
  {
    name: "Buster - Legetoej og snacks",
    link: "",
    pictureUrl: "",
  },
];
