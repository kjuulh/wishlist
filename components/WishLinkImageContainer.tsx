import styled from "styled-components";

export const WishLinkImageContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 20px;
  z-index: 20;
`;
