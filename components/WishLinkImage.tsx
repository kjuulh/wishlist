import styled from "styled-components";
import Image from "next/image";

export const WishLinkImage = styled(Image)`
  border-radius: 8px;
  object-fit: cover;
`;
