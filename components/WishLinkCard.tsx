import styled from "styled-components";

export const WishLinkCard = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 40px 10px 10px;
  border-radius: 10px;
  box-shadow: 2px 2px 16px rgba(0, 0, 0, 0.25);
  transition: box-shadow 200ms ease, padding 200ms ease;

  :hover {
    box-shadow: 2px 2px 32px rgba(0, 0, 0, 0.35);
  }

  :active {
    box-shadow: 2px 2px 32px rgba(0, 0, 0, 0.15);
  }
`;
